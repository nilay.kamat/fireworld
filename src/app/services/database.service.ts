import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { Observable } from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class DatabaseService {
    private postsCollection: any;
    public posts: Array<object>;

    constructor(private afs: AngularFirestore) {
        this.postsCollection = afs.collection('posts')
    }

    getPosts = (userId = null) => {
        return !userId ? this.postsCollection.snapshotChanges() : 
        this.afs.collection('posts', ref => ref.where('userId', '==', userId)).snapshotChanges();
    }

    getPost = (id: string) => {
        if(this.posts){
            let post = this.posts.filter(post => {
                return post['id'] == id;
            });
            return post[0] || null;
        } else {
            return this.afs.collection('posts').doc(id).get()
            // write logic to fetch new post from db
        }
    }
}

